document.querySelector('.tabs').addEventListener('click', (event) => {
  document.querySelector('.active').classList.remove('active')
  event.target.classList.add('active')
  const data = event.target.getAttribute('data-tab')
  document.querySelector('.text-about.active').classList.remove('active')
  document
    .querySelector(`.text-about[data-tab="${data}"]`)
    .classList.add('active')
})
